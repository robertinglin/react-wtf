import { Component, createElement } from 'react';
import { getState, addGlobalListener } from './Setup';
import Immutable from 'immutable';

const connectComponent = (mapStateToProps, options = {
    store: 'global'
}) => {
    let stores = [];
    if (options.store === 'global') {
        stores.push({
            addListener: addGlobalListener,
            getState
        });
    } else {
        if (!Array.isArray(options.store)) {
            stores.push(options.store);
        } else {
            stores = options.store;
        }
    }
    return (WrappedComponent) => {
        class connectComponent extends Component {
            constructor(props) {
                super(props);
                this.propsFactory = (props) => {
                    let storeObj = {};
                    if (options.store === 'global') {
                        storeObj = stores[0].getState();
                    }
                    else {
                        stores.forEach(store => {
                            storeObj[store.storeName] = store.getState();
                        });
                    }
                    return { ...props, ...mapStateToProps(storeObj, props)} 
                };
                this.state = {
                    componentProps: this.propsFactory(props)
                };
                this.listenerDeregisters = [];
                stores.forEach(this.registerListener.bind(this));
            }

            componentWillReceiveProps(props) {
                this.setState({
                    componentProps:this.propsFactory(props)
                });
            }

            componentWillUnmount() {
                this.listenerDeregisters.forEach(deregister => deregister());
            }

            registerListener(store) {

                this.listenerDeregisters.push(store.addListener.apply(store, [() => {
                    this.setState({
                        componentProps:this.propsFactory(this.props)
                    });
                }]));
            }

            render() {
                return createElement(WrappedComponent, this.state.componentProps);
            }
        }
        connectComponent.displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
        return connectComponent;
    }
}


export default connectComponent;