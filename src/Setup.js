import { Setup } from 'whattheflux';

let globalState = {};
let isSetup = false;
let wtf;

export const getState = () => {
    if (isSetup) {
        return globalState;
    }
    throw new Error('WTF not setup');
}

export const addGlobalListener = (listener) => {
    return wtf.addGlobalListener(listener)
}

export default (stateObj = {}) => {
    globalState = stateObj;
    wtf = Setup(globalState);
    isSetup = true;
    return wtf;
}
