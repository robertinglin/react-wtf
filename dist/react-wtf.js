(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"), require("whattheflux"), require("immutable"));
	else if(typeof define === 'function' && define.amd)
		define(["react", "whattheflux", "immutable"], factory);
	else if(typeof exports === 'object')
		exports["react-wtf"] = factory(require("react"), require("whattheflux"), require("immutable"));
	else
		root["react-wtf"] = factory(root["react"], root["whattheflux"], root["immutable"]);
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addGlobalListener = exports.getState = undefined;

var _whattheflux = __webpack_require__(4);

var globalState = {};
var isSetup = false;
var wtf = void 0;

var getState = exports.getState = function getState() {
    if (isSetup) {
        return globalState;
    }
    throw new Error('WTF not setup');
};

var addGlobalListener = exports.addGlobalListener = function addGlobalListener(listener) {
    return wtf.addGlobalListener(listener);
};

exports.default = function () {
    var stateObj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    globalState = stateObj;
    wtf = (0, _whattheflux.Setup)(globalState);
    isSetup = true;
    return wtf;
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Setup = undefined;

var _connectComponent = __webpack_require__(2);

var _connectComponent2 = _interopRequireDefault(_connectComponent);

var _Setup = __webpack_require__(0);

var _Setup2 = _interopRequireDefault(_Setup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _connectComponent2.default;
var Setup = exports.Setup = _Setup2.default;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(3);

var _Setup = __webpack_require__(0);

var _immutable = __webpack_require__(5);

var _immutable2 = _interopRequireDefault(_immutable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var connectComponent = function connectComponent(mapStateToProps) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
        store: 'global'
    };

    var stores = [];
    if (options.store === 'global') {
        stores.push({
            addListener: _Setup.addGlobalListener,
            getState: _Setup.getState
        });
    } else {
        if (!Array.isArray(options.store)) {
            stores.push(options.store);
        } else {
            stores = options.store;
        }
    }
    return function (WrappedComponent) {
        var connectComponent = function (_Component) {
            _inherits(connectComponent, _Component);

            function connectComponent(props) {
                _classCallCheck(this, connectComponent);

                var _this = _possibleConstructorReturn(this, (connectComponent.__proto__ || Object.getPrototypeOf(connectComponent)).call(this, props));

                _this.propsFactory = function (props) {
                    var storeObj = {};
                    if (options.store === 'global') {
                        storeObj = stores[0].getState();
                    } else {
                        stores.forEach(function (store) {
                            storeObj[store.storeName] = store.getState();
                        });
                    }
                    return _extends({}, props, mapStateToProps(storeObj, props));
                };
                _this.state = {
                    componentProps: _this.propsFactory(props)
                };
                _this.listenerDeregisters = [];
                stores.forEach(_this.registerListener.bind(_this));
                return _this;
            }

            _createClass(connectComponent, [{
                key: 'componentWillReceiveProps',
                value: function componentWillReceiveProps(props) {
                    this.setState({
                        componentProps: this.propsFactory(props)
                    });
                }
            }, {
                key: 'componentWillUnmount',
                value: function componentWillUnmount() {
                    this.listenerDeregisters.forEach(function (deregister) {
                        return deregister();
                    });
                }
            }, {
                key: 'registerListener',
                value: function registerListener(store) {
                    var _this2 = this;

                    this.listenerDeregisters.push(store.addListener.apply(store, [function () {
                        _this2.setState({
                            componentProps: _this2.propsFactory(_this2.props)
                        });
                    }]));
                }
            }, {
                key: 'render',
                value: function render() {
                    return (0, _react.createElement)(WrappedComponent, this.state.componentProps);
                }
            }]);

            return connectComponent;
        }(_react.Component);

        connectComponent.displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
        return connectComponent;
    };
};

exports.default = connectComponent;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ })
/******/ ]);
});