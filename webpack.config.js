var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'react-wtf.js',
        library: 'react-wtf',
        libraryTarget: 'umd'
    },
    externals: ['react', 'whattheflux', 'immutable'],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        // new webpack.optimize.UglifyJsPlugin(),
        // new webpack.optimize.OccurrenceOrderPlugin(),
    ]
};